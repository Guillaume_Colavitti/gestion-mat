<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Ressource;
use App\Entity\User;
use App\Repository\RessourceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api', name: 'api')]
class ApiController extends AbstractController
{
//    #[Route('/ressources', name: 'ressources', methods:"GET")]
//    public function ressources(RessourceRepository $ressourceRepository)
//    {
//        return $this->json($ressourceRepository->findAll(), 200, [],['groups' => 'ressources:read'] );
//    }

    #[Route('/users', name: 'users', methods:"GET")]
    public function users(UserRepository $userRepository): Response
    {
        return $this->json($userRepository->findAll(), 200, [],['groups' => 'users:read'] );
    }

    #[Route('/users/{id}', name: 'users_by_id', methods:"GET")]
    public function users_by_id(User $user, UserRepository $userRepository): Response
    {
        return $this->json($userRepository->find($user), 200, [],['groups' => 'users:read'] );
    }

    #[Route('/users', name: 'users_store', methods:"POST")]
    public function users_store(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, ValidatorInterface $validator):Response
    {
        $jsonRecu = $request->getContent();

        try{
            $user = $serializer->deserialize($jsonRecu, User::class, 'json');

            $errors = $validator->validate($user);
            if(count($errors) > 0){
                return $this->json($errors, 400);
            }
            $em->persist($user);
            $em->flush();


            return $this->json($user, 201, [], ['groups' => 'users:read']);
        } catch(NotEncodableValueException $e){
            return $this->json([
                'status'=> 400,
                'message'=>$e->getMessage()
            ], 400);
        }
    }

    #[Route('/users/{id}', name: 'users_update', methods:"PUT")]
    public function users_update(User $user, Request $request, EntityManagerInterface $em, ValidatorInterface $validator):Response
    {
        $jsonRecu = json_decode($request->getContent());
        try {
            $errors = $validator->validate($user);
            if(count($errors) > 0){
                return $this->json($errors, 400);
            }
            if (!empty($jsonRecu->email)){
                $user->setEmail($jsonRecu->email);
            }
            if (!empty($jsonRecu->firstname)){
                $user->setFirstname($jsonRecu->firstname);
            }
            if (!empty($jsonRecu->lastname)){
                $user->setLastname($jsonRecu->lastname);
            }
            if (!empty($jsonRecu->password)){
                $user->setPassword(password_hash($jsonRecu->password, PASSWORD_DEFAULT));
            }
            $em->persist($user);
            $em->flush();
        } catch (NotEncodableValueException $e){
            return $this->json([
                'status'=> 400,
                'message'=>$e->getMessage()
            ], 400);
        }
        return $this->json($user, 201, [], ['groups' => 'users:read']);
    }

    #[Route('/users/{id}/add/group/{group_id}', name: 'users_update_add_group', methods:"POST")]
    #[Entity('group', expr:'repository.find(group_id)')]
    public function users_update_group(User $user, Group $group, Request $request, EntityManagerInterface $em, ValidatorInterface $validator):Response
    {
        $user->setGroupId($group);

        $em->persist($user);
        $em->flush();

        return $this->json($user, 201, [], ['groups' => 'users:read']);
    }


    #[Route('/users/{id}', name: 'users_delete', methods:"DELETE")]
    public function users_delete(EntityManagerInterface $em, User $user):Response
    {
        $em->remove($user);
        $em->flush();

        return $this->json($user, 204, [], ['groups' => 'users:read']);
    }
}
