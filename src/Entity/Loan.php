<?php

namespace App\Entity;

use App\Repository\LoanRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LoanRepository::class)
 */
class Loan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $finished_at;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $returned_at;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="loans")
     */
    private $user_id;

    /**
     * @ORM\ManyToOne(targetEntity=Ressource::class, inversedBy="loans")
     */
    private $ressource_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getFinishedAt(): ?\DateTimeImmutable
    {
        return $this->finished_at;
    }

    public function setFinishedAt(\DateTimeImmutable $finished_at): self
    {
        $this->finished_at = $finished_at;

        return $this;
    }

    public function getReturnedAt(): ?\DateTimeImmutable
    {
        return $this->returned_at;
    }

    public function setReturnedAt(\DateTimeImmutable $returned_at): self
    {
        $this->returned_at = $returned_at;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getRessourceId(): ?Ressource
    {
        return $this->ressource_id;
    }

    public function setRessourceId(?Ressource $ressource_id): self
    {
        $this->ressource_id = $ressource_id;

        return $this;
    }
}
