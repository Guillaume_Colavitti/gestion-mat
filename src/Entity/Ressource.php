<?php

namespace App\Entity;

use App\Repository\RessourceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=RessourceRepository::class)
 */
class Ressource
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"ressources:read"})
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable="true")
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"ressources:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity_total;

    /**
     * @ORM\OneToMany(targetEntity=Loan::class, mappedBy="ressource_id")
     */
    private $loans;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, mappedBy="group_id")
     */
    private $groups;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, inversedBy="ressources")
     */
    private $ressource_id;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="ressource_id")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="ressources")
     */
    private $category_id;

    public function __construct()
    {
        $this->loans = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->ressource_id = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->category_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantityTotal(): ?int
    {
        return $this->quantity_total;
    }

    public function setQuantityTotal(int $quantity_total): self
    {
        $this->quantity_total = $quantity_total;

        return $this;
    }

    /**
     * @return Collection|Loan[]
     */
    public function getLoans(): Collection
    {
        return $this->loans;
    }

    public function addLoan(Loan $loan): self
    {
        if (!$this->loans->contains($loan)) {
            $this->loans[] = $loan;
            $loan->setRessourceId($this);
        }

        return $this;
    }

    public function removeLoan(Loan $loan): self
    {
        if ($this->loans->removeElement($loan)) {
            // set the owning side to null (unless already changed)
            if ($loan->getRessourceId() === $this) {
                $loan->setRessourceId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->addGroupId($this);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->groups->removeElement($group)) {
            $group->removeGroupId($this);
        }

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getRessourceId(): Collection
    {
        return $this->ressource_id;
    }

    public function addRessourceId(Group $ressourceId): self
    {
        if (!$this->ressource_id->contains($ressourceId)) {
            $this->ressource_id[] = $ressourceId;
        }

        return $this;
    }

    public function removeRessourceId(Group $ressourceId): self
    {
        $this->ressource_id->removeElement($ressourceId);

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addRessourceId($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->removeElement($category)) {
            $category->removeRessourceId($this);
        }

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategoryId(): Collection
    {
        return $this->category_id;
    }

    public function addCategoryId(Category $categoryId): self
    {
        if (!$this->category_id->contains($categoryId)) {
            $this->category_id[] = $categoryId;
        }

        return $this;
    }

    public function removeCategoryId(Category $categoryId): self
    {
        $this->category_id->removeElement($categoryId);

        return $this;
    }
}
