<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Group;
use App\Entity\Ressource;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RessourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('label')
            ->add('image')
            ->add('description')
            ->add('quantity_total')
            ->add('groups', EntityType::class, [
                'class' => Group::class,
                'choice_label' => 'label',
                'multiple' => true,
            ])
            // ->add('ressource_id')
            ->add('categories', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'label',
                'multiple' => true,
            ])
            // ->add('category_id')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ressource::class,
        ]);
    }
}
