<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Group;
use App\Entity\Loan;
use App\Entity\Ressource;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        for ($i = 0; $i < 20; $i++) {
            $ressource = new Ressource();
            $ressource->setLabel('Appareil '.$i);
            $ressource->setQuantityTotal(mt_rand(10, 100));
            $ressource->setDescription("description de l'appareil ".$i);
            $manager->persist($ressource);

            $user = new User();
            $user->setFirstname("Zinedine");
            $user->setLastname("Zidane");
            $user->setEmail("zinedine".$i."@gmail.com");
            $user->setPassword(password_hash("zizoumdp".$i, PASSWORD_DEFAULT));
            $manager->persist($user);

            
        }

        for($i = 0; $i <5; $i++){
            $category = new Category();
            $category->setLabel("Catégorie n°".$i);
            $manager->persist($category);

            $group = new Group();
            $group->setLabel("Groupe n°".$i);
            $manager->persist($group);
        }

        $manager->flush();
    }
}
