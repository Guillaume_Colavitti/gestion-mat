<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211208133805 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category_ressource (category_id INT NOT NULL, ressource_id INT NOT NULL, INDEX IDX_54FF977412469DE2 (category_id), INDEX IDX_54FF9774FC6CD52A (ressource_id), PRIMARY KEY(category_id, ressource_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE group_ressource (group_id INT NOT NULL, ressource_id INT NOT NULL, INDEX IDX_532C19A5FE54D947 (group_id), INDEX IDX_532C19A5FC6CD52A (ressource_id), PRIMARY KEY(group_id, ressource_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ressource_group (ressource_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_F954C041FC6CD52A (ressource_id), INDEX IDX_F954C041FE54D947 (group_id), PRIMARY KEY(ressource_id, group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ressource_category (ressource_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_48FDA01BFC6CD52A (ressource_id), INDEX IDX_48FDA01B12469DE2 (category_id), PRIMARY KEY(ressource_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category_ressource ADD CONSTRAINT FK_54FF977412469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_ressource ADD CONSTRAINT FK_54FF9774FC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_ressource ADD CONSTRAINT FK_532C19A5FE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_ressource ADD CONSTRAINT FK_532C19A5FC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ressource_group ADD CONSTRAINT FK_F954C041FC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ressource_group ADD CONSTRAINT FK_F954C041FE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ressource_category ADD CONSTRAINT FK_48FDA01BFC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ressource_category ADD CONSTRAINT FK_48FDA01B12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE loan ADD user_id_id INT DEFAULT NULL, ADD ressource_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE loan ADD CONSTRAINT FK_C5D30D039D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE loan ADD CONSTRAINT FK_C5D30D03EBD01AD3 FOREIGN KEY (ressource_id_id) REFERENCES ressource (id)');
        $this->addSql('CREATE INDEX IDX_C5D30D039D86650F ON loan (user_id_id)');
        $this->addSql('CREATE INDEX IDX_C5D30D03EBD01AD3 ON loan (ressource_id_id)');
        $this->addSql('ALTER TABLE user ADD group_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6492F68B530 FOREIGN KEY (group_id_id) REFERENCES `group` (id)');
        $this->addSql('CREATE INDEX IDX_8D93D6492F68B530 ON user (group_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE category_ressource');
        $this->addSql('DROP TABLE group_ressource');
        $this->addSql('DROP TABLE ressource_group');
        $this->addSql('DROP TABLE ressource_category');
        $this->addSql('ALTER TABLE loan DROP FOREIGN KEY FK_C5D30D039D86650F');
        $this->addSql('ALTER TABLE loan DROP FOREIGN KEY FK_C5D30D03EBD01AD3');
        $this->addSql('DROP INDEX IDX_C5D30D039D86650F ON loan');
        $this->addSql('DROP INDEX IDX_C5D30D03EBD01AD3 ON loan');
        $this->addSql('ALTER TABLE loan DROP user_id_id, DROP ressource_id_id');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6492F68B530');
        $this->addSql('DROP INDEX IDX_8D93D6492F68B530 ON user');
        $this->addSql('ALTER TABLE user DROP group_id_id');
    }
}
